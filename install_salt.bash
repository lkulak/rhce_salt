#!/bin/bash

set -e
set -u
set -o pipefail

# install saltstack
# setup local salt minion
# code placed to /srv/salt

curl -o bootstrap-salt.sh https://bootstrap.saltstack.com/stable/bootstrap-salt.sh
sudo sh bootstrap-salt.sh -P stable

sudo cat <<EOM>> /etc/salt/minion

master: localhost
file_client: local

file_roots:
  base:
    - /srv/salt/base

pillar_roots:
  base:
    - /srv/salt/pillar
EOM



