#!/bin/bash
set -e
set -u
set -o pipefail


# this script assumes there is nothing installed or you just installed base RHEL7 image (minimal install)
# assumption is that RHEL is using standard partion with /boot at 512Mib and / at 10Gib with 20Gib total disk

# we need to install cdrom repo to add basic deps as git to clone the project and install salt
# create dir to mount repo 
# please make sure for this part you have ISO mounted

base_packages=(git vim curl wget)

if [ ! -d /mnt/cdrom ]; then
 mkdir -p /mnt/cdrom
fi

if [[ ! $(ls /mnt/cdrom | wc -l) -gt  0 ]];then
 mount -t iso9660 -ro loop /dev/cdrom /mnt/cdrom
fi

#create base repo

cat <<EOM > /etc/yum.repos.d/media.repo
[media]
name="media repo"
baseurl=file:///mnt/cdrom
gpgcheck=0
enabled=1
EOM



for package in ${base_packages[@]}; do
 yum install -y ${package}
done



